# CloudWatch Log Group & Stream
resource "aws_cloudwatch_log_group" "cwl_loggroup" {
  name       = "${var.organization}-cloudtrail-logs-tflz"
  kms_key_id = var.aws_cloudtrail_kms_key.arn

  tags = local.tags
}

# CloudTrail Root Trail
resource "aws_cloudtrail" "root" {
  name           = var.aws_cloudtrail_bucket_name
  s3_bucket_name = var.aws_cloudtrail_bucket_name
  // sns_topic_name                = var.audit_sns_topic
  enable_log_file_validation    = true
  kms_key_id                    = var.aws_cloudtrail_kms_key.arn
  include_global_service_events = true
  is_multi_region_trail         = true
  is_organization_trail         = true

  # cloud_watch_logs_role_arn  = aws_iam_role.cloudtrail_cloudwatch_events_role.arn
  # cloud_watch_logs_group_arn = "${aws_cloudwatch_log_group.cwl_loggroup.arn}:*"

  event_selector {
    read_write_type           = "All"
    include_management_events = true

    data_resource {
      type   = "AWS::S3::Object"
      values = ["arn:aws:s3:::"]
    }
  }

  insight_selector {
    insight_type = "ApiCallRateInsight"
  }

  tags = local.tags
}
