data "aws_caller_identity" "current" {
  provider = aws
}

data "aws_caller_identity" "audit" {
  provider = aws.audit
}

data "aws_partition" "current" {
  provider = aws
}
