locals {
  tags_module = {
    Terraform               = true
    Terraform_Module        = "terraform-aws-organizations-cloudtrail"
    Terraform_Module_Source = "https://gitlab.com/tecracer-intern/terraform-landingzone/modules/terraform-aws-organizations-cloudtrail"
    Stage                   = var.stage
    System                  = var.system
  }
  tags = merge(local.tags_module, var.tags)
}
