
# IAM Role & Policy
locals {
  account_id = data.aws_caller_identity.audit.account_id
  partition  = data.aws_partition.current.partition
}

resource "aws_iam_role" "cloudtrail_cloudwatch_events_role" {
  name               = "${var.organization}-cloudtrail-events-role-tflz"
  assume_role_policy = data.aws_iam_policy_document.cwl_assume_policy.json

  tags = local.tags
}

resource "aws_iam_role_policy" "cwl_policy" {
  name   = "${var.organization}-cloudtrail-cloudwatch-events-policy-tflz"
  role   = aws_iam_role.cloudtrail_cloudwatch_events_role.id
  policy = data.aws_iam_policy_document.cwl_policy.json
}

data "aws_iam_policy_document" "cwl_assume_policy" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "cwl_policy" {
  statement {
    effect  = "Allow"
    actions = ["logs:CreateLogStream"]

    resources = [
      "arn:${local.partition}:logs:${var.region}:${local.account_id}:log-group:${aws_cloudwatch_log_group.cwl_loggroup.name}:log-stream:*",
    ]
  }

  statement {
    effect  = "Allow"
    actions = ["logs:PutLogEvents"]

    resources = [
      "arn:${local.partition}:logs:${var.region}:${local.account_id}:log-group:${aws_cloudwatch_log_group.cwl_loggroup.name}:log-stream:*",
    ]
  }
}
