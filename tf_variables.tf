// Required for provider
variable "region" {
  type        = string
  description = "The region of the account"
}

variable "aws_cloudtrail_bucket_name" {
  type        = string
  description = "ID of the AWS CloudTrail Bucket"
}

// Organization specific tags
variable "organization" {
  type        = string
  description = "Complete name of the organisation"
}

variable "system" {
  type        = string
  description = "Name of a dedicated system or application"
}

variable "stage" {
  type        = string
  description = "Name of a dedicated system or application"
}

variable "aws_cloudtrail_kms_key" {
  description = "KMS Key Object for CloudTrail Encryption"
}

variable "CIS_Metric_Alarm_Namespace" {
  default = "CisLogMetrics"
}

// Tags
variable "tags" {
  type        = map(string)
  description = "Tag that should be applied to all resources."
  default     = {}
}

variable "audit_email_subscriptions" {
  type        = list(string)
  description = "A list of emails that want to subscribe the Audit SNS Topic"
  default     = []
}
